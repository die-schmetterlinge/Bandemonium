# Bandemonium

### Bandemonium is musical pandemonium.
It's a story about a time-traveling band lost in the eighties.
Drenched in synths, they arrive at a place where they are babies.
In this place, electro bass and flat drums rule the day.
Equipped with a midi-keyboard, they decide to stay.

Bandemonium is an 80's themed synth/drum/bass midi player built using
Franz, a persistent event-log in ClojureScript inspired by
Kafka. Franz is built upon the amazing clojure.data.avl by Michał
Marczyk.

All the audio is done using WebAudio API & Webmidi and does not use
any javascript library.

## Acknowledgments

The beautiful intro music is the beatifully CreativeCommons Licensed song [80's Synth Rock (Guitar Improvisation) by Zombie Fish](https://soundcloud.com/zombie-fish-17/80s-synth-rock)

Graphics & the logo have been made using photoshop & great [80's design
tutorials by Abuzeedo](http://abduzeedo.com/80s-artwork-photoshop).

The icons in the header are graciously adapted from [a collection by Pham Thi Dieu Linh](https://thenounproject.com/phdieuli/collection/music-instrument/).

## Run development

- Start a repl with `lein repl`
- run `(go)`, this will start figwheel and other bootstrapping stuff
- If you want a cljs repl, run `cljs-repl` from the `user` namespace

## Run production
    lein with-profile "prod" uberjar



## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
