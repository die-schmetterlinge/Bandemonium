(defproject bandemonium "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0-RC3"]
                 [org.clojure/clojurescript "1.7.170"]
                 [franz "0.1.4-SNAPSHOT"]
                 [ring "1.4.0"]
                 [compojure "1.4.0"]
                 [ring-transit "0.1.4"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [com.h2database/h2 "1.4.190"]]

  :plugins [[lein-cljsbuild "1.1.1"]]

  :source-paths ["src/main/clj" "src/main/cljs"]
  :test-paths ["src/test/clj" "src/test/cljs"]
  :resource-paths ["src/main/resources"]

  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.2.1"]
                                  [figwheel-sidecar "0.5.0-2"]
                                  [cljs-ajax "0.5.1"]
                                  [reagent "0.5.1"]
                                  [org.clojars.triss/midi-data "0.2.0-SNAPSHOT"]]
                   :source-paths ["src/dev/clj" "src/dev/cljs"]
                   :resource-paths ["src/dev/resources"]}
             :uberjar {:main bandemonium.core
                       :omit-source true
                       :aot :all
                       :source-paths ["src/main/clj" "src/main/cljs"]
                       :uberjar-name "bandemonium.jar"}}
  :cljsbuild {:builds
              {:dev {:source-paths ["src/dev/cljs" "src/main/cljs"]
                     :figwheel {:on-jsload bandemonium.dev/on-jsload}
                     :compiler {:output-to "src/main/resources/public/js/app.js"
                                :source-map "src/main/resources/public/js/app.js"
                                :asset-path "js/out"
                                :main bandemonium.core
                                :optimizations :none}}
               :prod
               {:source-paths ["src/main/cljs"]
                :jar true
                :compiler {:output-to "src/main/resources/public/js/app.js"
                           :asset-path "js/out"
                           :main bandemonium.core
                           :optimizations :advanced
                           :pretty-print false}}}}

  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

  :figwheel {:css-dirs ["src/main/resources/public/css"]}
  :clean-targets ^{:protect false} [:target-path])
