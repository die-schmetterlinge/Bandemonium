(ns bandemonium.playback
  (:require [reagent.core :as r]
            [bandemonium.service :as service]
            [bandemonium.playback.soundfonts :as sf]
            [bandemonium.playback.synth :as s]
            [bandemonium.playback.drums :as d]))
(def all-sounds
  {:drums d/all-sounds
   :bass sf/all-sounds})

(def midi->sound
  {:drums d/midi->sound
   :bass (fn [midi] (sf/midi->sound (- midi 24)))})

(def instrument->channel
  {:drums 0
   :bass 1
   :synth 2})
(def channel->instrument
  {0 :drums
   1 :bass
   2 :synth})

(def context (cond
               js/window.AudioContext (js/window.AudioContext.)
               js/window.webkitAudioContext (js/webkitAudioContext.)))

(def sounds (r/atom {}))
(def current-instrument (r/atom nil))

(def loading-sounds? (r/atom false))

(defn add-to-sounds [instrument note]
  (fn [audio-data]
    (.decodeAudioData context audio-data
                      (fn [buffer]
                        (swap! sounds assoc-in [instrument note] buffer)
                        (if (= (count (instrument all-sounds))
                               (count (get @sounds instrument)))
                          (reset! loading-sounds? false))))))

(defn load-instrument! [instrument]
  (reset! current-instrument instrument)
  (when-not (get-in @sounds [instrument])
    (println "Loading instrument:" instrument)
    (reset! loading-sounds? true)
    (doseq [note (all-sounds instrument)]
      (service/load-sound! instrument note ".mp3" (add-to-sounds instrument note)))))

(defn play-sound [s]
  (let [source (.createBufferSource context)]
    (set! (.-buffer source) s)
    (.connect source (.-destination context))
    (.start source 0)))


(defn play-midi [{:keys [midi-msg note velocity channel]}]
  (when (= :note-on midi-msg)
    (let [instrument (channel->instrument channel)]
      (when instrument
        (if (= :synth instrument)
          (s/handle-event note velocity)
          (when (pos? velocity) ; mp3
            (when-let [sound (get-in @sounds [instrument
                                              ((get midi->sound instrument) note)])]
              (play-sound sound))))))))
