(ns bandemonium.service
  (:require [ajax.core :as ajax]
            [ajax.protocols :refer [-body]]))

(defn load-sound! [instrument note extension cb]
  (ajax/ajax-request {:uri (str "/soundfont/" (name instrument) "/" (name note) extension)
                      :method "GET"
                      :api (js/XMLHttpRequest.)
                      :error-handler (fn [resp] (println [:error (:status-text resp)]))
                      :handler #(cb (second %))
                      :response-format {:type :arraybuffer
                                        :description "ok"
                                        :read -body}}))

(defn get-song [song-name cb]
  (ajax/POST "/api/song"
             {:params {:song-name song-name}
              :handler cb
              :error-handler (fn [data]
                               (.error js/console "Song" song-name "not found." data))}))

(defn store-song [song-name data cb]
  (ajax/PUT "/api/song"
            {:params {:song-name song-name
                      :data data}
             :handler cb
             :error-handler (fn [data]
                              (.error js/console "Could not store song :( " data))}))
