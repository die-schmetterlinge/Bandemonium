(ns bandemonium.util
  (:require [midi-data.core :as midi-data]))

(defn iterator->seq [iterator]
  (loop [o (.next iterator) result (lazy-seq)]
    (if (.-done o)
      result
      (recur (.next iterator) (cons (.-value o) result)))))

(defn midi-event->midi-map [event]
  (midi-data/decode (.reduce (.-data event) (fn [acc x] (conj acc x)) [])))
