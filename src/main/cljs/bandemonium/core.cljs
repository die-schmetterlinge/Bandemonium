(ns bandemonium.core
  (:require [reagent.core :as r]
            [bandemonium.audio :as audio]
            [bandemonium.playback :as p]
            [bandemonium.keyboard :as k]
            [bandemonium.playback.synth :as s]
            [bandemonium.virtual-kb :as virtkb]
            [franz.core :as franz]
            [bandemonium.util :as util]))

(enable-console-print!)

(def sample-rate 10)

(def midi-events (r/atom []))

(def realtime-topic (franz/topic))
(def recording-topic (franz/topic))
(def replay-topic (franz/topic))

(def countdown (r/atom nil))

(def sample-id
  (do (js/clearInterval sample-id)
      (js/setInterval (fn []
                        (franz/tick! replay-topic :play)
                        (franz/tick! realtime-topic :record true))
                      sample-rate)))

(defn start-countdown!
  ([] (start-countdown! (fn [])))
  ([cb]
   (js/setTimeout #(reset! countdown 3) 0)
   (js/setTimeout #(swap! countdown dec) 1000)
   (js/setTimeout #(swap! countdown dec) 2000)
   (js/setTimeout #(do (reset! countdown nil) (cb)) 3000)))

(defn choose-instrument-component []
  [:div (doall
         (map (fn [i]
                [:div {:key i
                       :class (str "instrument mod-" (name i)
                                   (when (= i @p/current-instrument)
                                     (str " is-recording"
                                          (when (seq @k/current-keys) " is-pressed"))))
                       :on-click (fn [] (p/load-instrument! i))}])
              [:drums :bass]))
   [:div {:class (str "instrument mod-synth" (when (= :synth @p/current-instrument)
                                               (str " is-recording"
                                                    (when (seq @k/current-keys) " is-pressed"))))
          :on-click (fn [] (reset! p/current-instrument :synth)
                      (s/start-synth))}]])

(defn clean-sound-set [st]
  (into #{} (map
              (fn [[k v]]
                (assoc k :velocity
                         (apply min (map :velocity v))))
              (group-by #(select-keys %
                          [:midi-msg
                           :channel
                           :note])
                        st))))

(defn play-component []
  (.addEventListener js/document "keydown"
                     #(k/handle-keydown % (fn [note]
                                            (franz/send!
                                             realtime-topic
                                             {:midi-msg :note-on
                                              :channel (p/instrument->channel @p/current-instrument)
                                              :note note
                                              :velocity 127})
                                            (franz/flush! realtime-topic :play))))
  (.addEventListener js/document "keyup"
                     #(k/handle-keyup % (fn [note]
                                          (franz/send!
                                           realtime-topic
                                           {:midi-msg :note-on
                                            :channel (p/instrument->channel @p/current-instrument)
                                            :note note
                                            :velocity 0})
                                          (franz/flush! realtime-topic :play))))
  (.addEventListener js/document "keydown"
                     (fn [e]
                       (let [keycode (.-keyCode e)]
                         (cond
                           (= 32 keycode) (do
                                            (reset! replay-topic (franz/log-from-offsets (merge-with
                                                                                           (fn [[k1 v1]
                                                                                                [k2 v2]]
                                                                                             [k1 (clean-sound-set (clojure.set/union v1 v2))])
                                                                                           (into {} (franz/read-all @replay-topic))
                                                                                           (into {} (franz/read-all @recording-topic)))))
                                            (reset! recording-topic (franz/log)))
                           (= 27 keycode) (do (reset! recording-topic (franz/log))
                                              (reset! replay-topic (franz/log)))))))
  (fn []
    [:div
     [:header.top-bar
      [:div.top-bar-logo]]
     [:div.main
      [choose-instrument-component]
      (if (nil? @p/current-instrument)
        [:div.notification-bar "Please select an instrument"]
        (if (or
             (not @p/loading-sounds?)
             (= @p/current-instrument :synth))
          [:div
           [k/choose-layout-component]
           [:div.notification-bar "[Space] to restart loop, [ESC] to clear loop "]
           [virtkb/virtual-kb k/current-keys]]
          [:div.notification-bar "Loading sounds... please wait."]))]]))

(defn handle-event [event]
  (let [msg (util/midi-event->midi-map event)]
    (when (#{:note-on :note-off} (:midi-msg msg))
      (franz/send! realtime-topic (assoc msg :channel (p/instrument->channel @p/current-instrument)))
      (franz/flush! realtime-topic :play))))

(def current-page (r/atom :start))

(defn start-component []
  (letfn
      [(listener [k]
         (when (= 13 (.-keyCode k))
           (reset! current-page :main)))]
    (r/create-class
     {:component-will-mount
      #(.addEventListener js/document "keydown" listener)

      :component-will-unmount
      #(.removeEventListener js/document "keydown" listener)

      :reagent-render
      (fn []
        [:div.welcome
         [:h1.welcome-text
          "Press "
          [:span.welcome-text-blink "[Enter]"]
          " to start"]
         [:div
          [:audio {:autoplay true
                   :controls true
                   :style {:opacity 0}
                   :src "survivor.mp3"
                   :type "audio/mpeg"}]
          [:iframe {:src "survivor.mp3"
                    :style {:opacity 0}
                    :allow "autoplay"}]]])})))

(defn root-component []
  (if (= @current-page :start)
    [start-component]
    [play-component]))

(defn ^:export main []
  (audio/init! handle-event)
  (franz/subscribe! realtime-topic
                    :show-note
                    (fn [log position]
                      (let [[o [k v]] (franz/read log position)]
                        (swap! midi-events into v)
                        (if o
                          (inc o)
                          (count log)))))
  (franz/subscribe! realtime-topic
                    :play
                    (fn [log position]
                      (let [[o [k v]] (franz/read log position)]
                        (p/play-midi v)
                        (if o
                          (inc o)
                          (count log))))
                    {:auto-schedule false})
  (franz/subscribe! realtime-topic
                    :record
                    (fn [log position]
                      (let [events (franz/read-all log position)
                            last-offset (if-let [o (-> events last first)]
                                          (inc o)
                                          (count log))
                            k (when (empty? events) :empty)
                            msg (clean-sound-set (into #{} (map (comp second second) events)))]
                        (franz/send!
                         recording-topic
                         k
                         msg)
                        last-offset))
                    {:auto-schedule false})
  (franz/subscribe! replay-topic
                    :play
                    (fn [log position]
                      (let [[k v] (nth log position)]
                        (dorun (map p/play-midi v))
                        (inc position)))
                    {:auto-schedule false})
  (r/render-component
   [root-component]
   (js/document.getElementById "app")))
