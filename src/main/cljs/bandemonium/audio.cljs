(ns bandemonium.audio
  (:require [bandemonium.util :as util]))

(defn- debug [msg]
  (js/console.log msg))

(def midi-access (atom nil))
(def midi-inputs)
(def midi-output)

(defn- midi-io->map [io]
  (->> io
       (.values)
       util/iterator->seq
       (mapv #(hash-map :id (.-id %) :name (.-name %)))))

(defn list-midi-inputs []
  (->> @midi-access .-inputs midi-io->map))

(defn list-midi-outputs []
  (->> @midi-access .-outputs midi-io->map))

(defn has-midi-device? [ma]
  (pos? (-> ma .-inputs .-size)))

(defn on-midi-fail [e]
  (.error js/console "Could not initialize MIDI access" e))

(defn hook-up-midi-input! [handler]
  (letfn [(reg-handler []
            (doseq [midi-input midi-inputs]
              (set! (.-onmidimessage midi-input) handler)))]
    reg-handler))

(defn on-midi-success [handler]
  (fn [ma]
    (reset! midi-access ma)
    (when (has-midi-device? ma)
      ;; TODO: hook-up selected MIDI input/output instead of first
      (do
        (set! midi-inputs (util/iterator->seq (-> ma .-inputs (.values))))
        ((hook-up-midi-input! handler))))))

(defn init! [handler]
  (when js/navigator.requestMIDIAccess
    (-> (js/navigator.requestMIDIAccess)
        (.then (on-midi-success handler) on-midi-fail))))

(defn send-midi-event!
  ([event]
   (send-midi-event! event midi-output))
  ([event output]
   (.send output event)))
