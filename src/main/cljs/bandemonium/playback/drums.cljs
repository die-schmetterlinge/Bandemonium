(ns bandemonium.playback.drums)

(def all-sounds [:SLAP_1 :SLAP_2 :SLAP_3 :SQSM-01 :SQSM-02 :SQSM-03 :SQSM-04 :SQSM-05 :SQSM-06 :SQSM-07 :SQSM-08 :SQSM-09 :SQSM-10 :SQSM-11 :SQSM-12 :TOM_01 :TOM_02 :TOM_03 :TOM_04 :TOM_05 :TOM_06 :TOM_07 :TOM_08 :TOM_09 :TOM_10 :TOM_11 :TOM_12 :TOM_13 :TOM_14 :TOM_15 :TOM_16 :TOM_17])

(def midi->sound
  (zipmap (range 48 84) all-sounds))
