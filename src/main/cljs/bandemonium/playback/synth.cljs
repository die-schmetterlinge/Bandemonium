(ns bandemonium.playback.synth)

(def context (cond
               js/window.AudioContext (js/window.AudioContext.)
               js/window.webkitAudioContext (js/webkitAudioContext.)))

(def envelope (.createGain context))

(def oscillators (atom {}))

(defn note->frequency [note]
  (* 440
     (.pow js/Math
           2
           (/ (- note 69) 12))))

(defn start-synth []
  (do
    (.connect envelope (.-destination context))
    (set! (-> envelope .-gain .-value) 0.0)))

(defn play-note [note]
  (let [oscillator (.createOscillator context)]
    (do
      (swap! oscillators assoc note oscillator)
      (set! (.-type oscillator) "sawtooth")
      (.setValueAtTime (.-frequency oscillator) 110 0)
      (.connect oscillator envelope)
      (.start oscillator 0)
      (.cancelScheduledValues (.-frequency oscillator) 0)
      (.setTargetAtTime (.-frequency oscillator) (note->frequency note) 0 0)
      (.cancelScheduledValues (.-gain envelope) 0)
      (.setTargetAtTime (.-gain envelope) 0.2 0 0.02))))

(defn stop-note [note]
  (let [oscillator (get @oscillators note)]
    (swap! oscillators dissoc note)
    (.stop oscillator 0)))

(defn handle-event [note velocity]
  (if (pos? velocity)
    (play-note note)
    (stop-note note)))
