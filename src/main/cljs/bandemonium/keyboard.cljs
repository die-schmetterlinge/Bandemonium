(ns bandemonium.keyboard
  (:require [reagent.core :as r]
            [goog.labs.userAgent.browser :as browser]))

(def current-keys (r/atom #{}))

;;  (zipmap [97 115 100 102 103 104 106 107 108] (range)) -- for key-press
(def key-layout
  (if (browser/isFirefox)
    (zipmap
      [65 90 83 88 67 70 86 71 66 72 78 77 75 188 76 190 191 49 81 50 87 51 69 82 53 84 54 89 85 56 73 57 79 48 80 219 61 221]
      (drop 49 (range)))
    (zipmap
      [65 90 83 88 67 70 86 71 66 72 78 77 75 188 76 190 191 49 81 50 87 51 69 82 53 84 54 89 85 56 73 57 79 48 80 219 187 221]
      (drop 49 (range)))))

(def continuous-layout
  (if (browser/isFirefox)
    (zipmap
      [90 90 88 67 86 66 78 77 188 190 191 65 83 68 70 71 72 74 75 76 59 222 81 87 69 82 84 89 85 73 79 80 219 221 49 50 51 52 53 54 55 56 57 48 173 61]
      (drop 48 (range)))
    (zipmap
      [90 88 67 86 66 78 77 188 190 191 65 83 68 70 71 72 74 75 76 186 222 220 81 87 69 82 84 89 85 73 79 80 219 221 49 50 51 52 53 54 55 56 57 48 189 187]
      (drop 48 (range)))))

(def current-layout (r/atom continuous-layout))

(defn set-keyboard-layout!
  "Layout style must be :continuous or :piano"
  [layout-style]
  (reset! current-layout (if (= :continuous layout-style)
                           continuous-layout
                           key-layout)))

(defn choose-layout-component []
  (if (= key-layout @current-layout)
    [:button.layout-change
     {:on-click #(set-keyboard-layout! :continuous)}
     "Switch to continuous layout"]
    [:button.layout-change
     {:on-click #(set-keyboard-layout! :piano)}
     "Switch to piano layout"]))


(defn handle-keyup [e cb]
  (let [key (@current-layout (.-keyCode e))]
    (when (and key (@current-keys key))
      (swap! current-keys disj key)
      (cb key))))

(defn handle-keydown [e cb]
  (let [key (@current-layout (.-keyCode e))]
    (when (and key (not (@current-keys key)))
      (swap! current-keys conj key)
      (cb key))))
