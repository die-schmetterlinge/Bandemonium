(ns bandemonium.core
  (:require [bandemonium.server :as server]
            [bandemonium.persistence :as persistence])
  (:gen-class))


(defn start! []
  (persistence/init!)
  (server/start!))

(defn -main []
  (start!))
