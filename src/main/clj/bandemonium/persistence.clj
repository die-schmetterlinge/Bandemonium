(ns bandemonium.persistence
  (:require [clojure.java.jdbc :as j]
            [clojure.java.io :as io])
  (:import [java.io BufferedReader]
           [java.sql Clob]))

(def ^:dynamic *conn* {:classname "org.h2.Driver"
                       :subprotocol "h2"
                       :subname "file:./.data/bandemonium.db"
                       :user ""
                       :password ""})

(defn init! []
  (let [sql-script (slurp (io/resource "db/schema.sql"))]
    (when-not (seq (j/query *conn* ["SELECT * FROM information_schema.tables WHERE table_name = ?" "bandemonium"]))
      (j/execute! *conn* [sql-script]))))

(defn store-song [song-name data]
  (println song-name data)
  (j/insert! *conn* :song {:name song-name :payload data}))

(defn clob->string [^Clob clob]
  (with-open [rdr (BufferedReader. (.getCharacterStream clob))]
    (apply str (line-seq rdr))))

(defn get-song [song-name]
  (first
    (j/query *conn*
             ["SELECT * FROM song WHERE name = ?" song-name]
             :row-fn (fn [row] (update-in row [:payload] clob->string)))))
