(ns bandemonium.server
  (:require [ring.util.response :as response]
            [bandemonium.persistence :as persistence]
            [compojure.route :as route]
            [compojure.core :refer [defroutes GET PUT POST]]
            [ring.middleware.transit :as transit]
            [ring.adapter.jetty :as jetty])
  (:import [org.eclipse.jetty.server Server]))

(def server (atom nil))


(defroutes routes
  (GET "/" req (response/resource-response "index.html" {:root "public"}))
  (PUT "/api/song"
       {{:keys [song-name data]} :body :as req}
    (try
      (persistence/store-song song-name data)
      (catch Exception e
        {:status 400
         :body (str "Nope... \n" (.getMessage e))})))
  (POST "/api/song"
        {{:keys [song-name]} :body}
    (if-let [res (persistence/get-song song-name)]
      {:status 200 :body res}
      (response/not-found "No song found with that name :(")))
  (route/resources "/")
  (route/not-found "Not found"))



(def app
  (-> routes
      (transit/wrap-transit-response {:encoding :json})
      (transit/wrap-transit-body)))

(defn start! []
  (when-not @server
    (reset! server (jetty/run-jetty #'app {:join? false :port 8580}))))

(defn stop! []
  (when-let [s ^Server @server]
    (println "Stopping server")
    (.stop @server)
    (reset! server nil)))