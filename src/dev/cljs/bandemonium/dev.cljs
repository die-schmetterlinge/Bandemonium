(ns bandemonium.dev
  (:require [bandemonium.core :as core]))

(enable-console-print!)

(defn on-jsload []
  (core/main))
